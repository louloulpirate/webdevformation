//une chaine de caractere est similaire à un tableau de caractères
// on définit généralement une chaine comme du texte contenu entre guillemets ou apostrophes
let exemple = "coucou"
let exemple2 = 'aujourd\'hui' //si on utilise des ' pour délimiter notre string, on doit échapper les caractères ' dans notre string pour ne pas la fermer par inadvertance 

//pour concaténer des chaines ensemble on utilise + 
//on peut également concaténer des nombres avec des chaînes

let concatenation = "ceci est " + 1 + "chaine"
//équivaut à "ceci est une chaine"

//on peut également concaténer des variables contenant des chaines ou autres
let politesse = "Comment ça va " + exemple + " ?"
//équivaut à Comment ça va aujourd'hui ?

//à la place d'une concaténation on peut également utiliser les `` pur inserer des variables dans une chaine
//${} permet également d"éxécuter du code, des conditions ou des fonctions 
let politesse2 = `comment ça va ${exemple} ?`

//fonctions liées aux string :
//manipuler la casse (minuscule/majuscule)
//toUpperCase() permet de paqsser une chaine en majuscule
//toLowerCase() permet de passer une chaine en minuscule
let petitbonjour = "bonjour"
petitbonjouer.toUpperCase() // renvoir "BONJOUR"

let groscoucou = "COUCOU"



//etant donner que les chaines de caracteres sont représentées comme des tzableaux de caracteres , on peut accéder à un caractere en particulier à l'aide d'un indice numérique 

let phrase ="balkany est en liberté"
phrase[0] // renvoie "B"
phrase[6]// renvoie "y"
phrase[11]// renvoie ""
phrase[phrase.length - 1] //renvoie le dernier caractere "é"

//on peut également parcourir une chaine à laide d'un for
for (let i = 0;i < phrase.length; i++){
    console.log(phrase[i]) //affiche chaque caractere
}

//on peut séparer une chaine en sous parties à l'aide de split()
//split permet de séparer une chaine en sous parties délimitées par un caractère précis
//par exemple, pour récuperer tous les mots de notre phrase , on split la chaine au niveau des espaces

let mots = phrase.split(" ")
//mot contient ["Balkany", "est", "en", "liberté"]
let numero = "06.28.45.19.44"
numero.split(".") // renvoi ["06", "28", "45", "19", "44"]







