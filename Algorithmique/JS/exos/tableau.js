//un tableau est une structure de donneés de taille définie
//permettant de stocker un jeu de données de même type.
//En javascript (et en programmation en général) un tableau se déclare 
//à l'aide de crochets []
let tableauvide = [] //déclaration d'un tableau vide 
// un tableau contient des cases, chaque case contient une donnée
// chaque case est séparée par une virgule
//                     0          1          2
let listecourses = ['eponges', 'pates', 'sauce tomate']
// pour accéder aux données stockées dans le tableau on utilise un indice
listecourses[0] //contient "eponges"
listecourses[1] //contient "pates"
listecourses[2] //contient "sauce tomate"
listecourses[3] //renvoie undefined

//pour récupérer la taille d'un tableau on peut utiliser array.length
listecourses.length //renvoie 3
//le dernier éléméent d'un tableau se trouve à array [array.length - 1]

//pour parcourir un tableau , une bonne méthode est d'utiliser une boucle
//la boucle for semble toute indiquée
//un tableau commence à 0, donc on initialise i à 0
//on s'arrete juste avant la fin du tableau 
for (let i = 0; i < listecourses.length; i++){
    console.log(listecourses[i])
}
//pour ajouter un élément à la fin d'un tableau 
//on utilise array.push(element)
listecourses.push('sel') // ['eponges', 'pates', 'sauce tomate', 'sel']
listecourses[3] //renvoie "sel"

//pour retirer le dernier élement d'un tableau 
//on utilise array.pop()
listecourses.pop() //renvoie et supprime le dernier element du tableau
// ['eponges', 'pates', 'sauce tomate']

//ajouter un élément au début du tableau demande l'utilisation de 
//aray.unshift ('whisky')
//['whisky', 'eponges', 'pates', 'sauce tomate']

//supprimer le premier element du tableau se fait à l'aide de 
//array.shift
listecourses.shift() //renvoie et supprime 'whisky' de la liste 
//['eponges', 'pates', 'sauce tomate']
//array.splice() permet de raccorder, supprimer et remplacer des éléments dans un tableau
//pour remplacer un élément par un autre par un exemple
// à la case 1, je supprime l'élement, et insere l'élement "pates completes"
listecourses.splice(1, 1, 'pate completes')
//['eponges', 'pate complete', 'sauce tomate']
//on peut séparer les éléments à inserer par des virgules pour en inserer plusieurs 
listecourses.splice(1, 0, 'parmesan', 'pesto')
//['eponges', 'parmesan' ,'pesto', 'pates complete', 'sauce tomates']
//splice renvoie également tous les élements supprimés

//array.slice() renvoie une partie d'un tableau
//en faisant une copie des éléments à partir d'une borne inférieur 
//(incluse) jusqu'à une borne superieure (exlue)
listecourses.slice(1, 4) //renvoie ['parmesan', 'pesto', 'pates completes']