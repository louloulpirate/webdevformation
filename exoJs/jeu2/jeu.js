

class Personnage {
    constructor(nom, pv, force, xp){
        this.nom = nom;
        this.pv = pv;
        this.force = force;
        this.xp = 0;
    }

    description(){
        if (this.pv > 0) {
            return `Je m'appelle ${this.nom} et j'ai ${this.pv} points de vie.\nJ'ai ${this.force} de force et ${this.xp} points d'expérience.\n`;
        }
        else {
            return `${this.nom} erre dans les limbes de l'enfer...\n`;
        }
    }

    set_pv(nb_pv){
        if (nb_pv >= 0){
            this.pv += nb_pv;
        }
        else {
            this.pv += nb_pv;
            if (this.pv < 0) {
                this.pv = 0;
            }
        }
    }

    attaquer(cible){
        console.log(`${this.nom} attaque ${cible.nom}`);
        cible.set_pv(-this.force);
     console.log(`${cible.nom} perd ${this.force} pv`);
    }   
}

    //Animal extends Personnage{
   // Attributs:
    //    nb_dents: nombre positif
    //    nb_decibels: nombre positif
  //  Méthodes:
     //   Mordre: attaque qui dépent de nb_dents
   //     Intimider: attaque qui dépent de nb_decibels et de l'animal qui agit



class Animal extends Personnage {
    constructor (nom, pv, force, xp, nb_dents, nb_decibels){
        super (nom, pv, force, xp)
        this.nb_dents = nb_dents
        this.nb_decibels = nb_decibels
    }
    description(){
       if (this.pv > 0) {
           return `Je suis  ${this.nom}, j'ai ${this.pv} points de vie et ${this.nb_dents} dents bien aiguisées .\nJ'ai ${this.force} de force. Mon compte de points d'expérience s'élève à ${this.xp}.\n`;
       }
       else {
           return `${this.nom} erre dans les limbes de l'enfer...\n`;
       }
   }
   mordre(cible){
      cible.set_pv(-this.nb_dents)
      console.log(`${this.nom} a mordu ${cible.nom} et lui enlève ${this.nb_dents} point de vie. Il se vide de son sang`);
   }

   intimider(nb_force){
       if (this.nb_decibels >= 0) {
            this.force += this.nb_decibels
       }
       console.log(`Lance intimidation`)
       console.log(`Que je trépasse si je faiblis ! ma force augmente de 5`)
    }
   }
  
   class Licorne extends Animal {
    constructor (nom, pv, force, xp, nb_dents, nb_decibels){
        super (nom, pv, force, xp,)
    }
   }


















class Combattant extends Personnage{
    constructor(nom, pv, force, xp, arme, inventaire){
        super(nom, pv, force, xp);
        this.arme = arme;
        this.inventaire = inventaire;
    }

    description(){
        if (this.pv > 0) {
            return `Je m'appelle ${this.nom}, je suis un GUERRIER armé de ${this.arme} et ${this.inventaire} dans mon inventaire. J'ai ${this.pv} points de vie, ${this.force} de force et ${this.xp} points d'expérience.\n`;
        }
        else {
            return `${this.nom} erre dans les limbes de l'enfer...\n`;
        }
    }

    
}

const Animal1 = new Animal("Un Animal", 30, 15, 30, 20)
const Animal2 = new Licorne("Une licorne", 30, 15, 0, 35, 15)

const Valentin = new Combattant("Gandalf", 20, 30, 0, "excalibur", "rien");
const Thomas = new Personnage("Sauron", 40, 15, 0);
 const Louis = new Personnage("Bilbo", 40, 20, 50);


console.log(Animal1.description());
Animal2.intimider() 
console.log(Animal1.description());



// console.log(Valentin.description());
// console.log(Thomas.description());
// console.log(Louis.description());

// const nb_joueur = Number(prompt("Saisir le nombre de joueurs"));
// const liste_joueur = [];
// for (let i = 0; i < nb_joueur; i++) {
//     let nom = prompt(`Saisir son nom du ${i+1}e joueur`);
//     let pv = getRandNum(100);
//     let pm = getRandNum(100);
//     let force = getRandNum(100);
//     let perso = new Personnage(nom, pv, pm, force);
//     liste_joueur.push(perso);
//     console.log(liste_joueur[i].description());
// }

// console.log(getRandNum(5));

