//DOM = Document Object Model
// le DOM est un ensemble de données représentant la page web et ses éléments
//il est assemblé par le navigateur au moment de l'interprétation du HTML
//Un object javascript est donc utilisé pour représenter chaque élément de notre page
//Un objet document définit l'entiereté de la page
//Il contient la représentation en Javascript de chacune des balises textuelles
//dans la variable document se trouve le DOM
//document est initialisé lors de la création de la page et contient toutes les informations sur la page elle meme

console.log(document);

//avec ce document de type htmlDOcument on peut effectuer certaines manipulation sur le doncument lui même ou même ses enfants
//on peut effectuer ces manipulations au travers des nombreuses propriétés et méthodes de l'objet document

console.log(document.body); //document.body contiendra nul si body n'a pas été interprété par le navigateur au moment de l'exécution de notre script
//on peut par exple , pour gérer ce problème, déplacer le script en bas de notre body pour s'assurer qu'il soit bien lu après l'interprétation de toute notre page
//plus tard, on utilisera des méthodes plus "sophistiquées" comme les évènements

//chaque élément du DOM étant représenté plus ou moins de la même façon sous la forme d'un HTMLElement, ils possèdent donc tous des propriétés en commun , parmi celles ci se trouvent les propriété children et childNodes
//dans children se trouvent tous les HTMLElement contenus dans l'élément ciblé
console.log(document.body.children);
//dans childNodes se trouvent tous les noeud (Elements+Texte) contenus dans l'élément ciblé
console.log(document.body.childNodes)

//il est possible de cibler plus précisément certains éléments du DOM et ainsi intéragir avec
//Pour récuperer tous les éléméent d'un même nom de balise, on peut utiliser getElementsByTagName
const liElements = document.getElementsByTagName('li');
//getElementByTagName('element') renvoie donc une HTMLCollection contenant tous les éléments <element> de notre document
console.log(liElements); //liElements.length pourrait donc nous renvoyer le nombre de <li> présents dans notre page

//dans la même veine que getElementsByTagName se trouve getElementsByClassName qui effectue le même comportement mais avec un nom de classe et pas de balise
const elementElements = document.getElementsByClassName('element');
console.log(elementElements); //tous les élements possédants la classe 'element' seront renvoyer dans une HTMLCollection

//il existe une autre méthode permettant de récupérer un ou plusieurs éléméent via un selecteur css
const liElement = document.querySelectorAll("ul > li");
//querySelectorAll renvoie tous les candidats séléectionnés par le selecteur css
//querySelector renvoie uniquement le premier trouvé correspondant au selecteur css
console.log(liElement);
const li2 = document.querySelector("#li2")
console.log(li2);

//querySelectorAll renvoie tous les candidats sélectionnés par le selecteur css
//querySelector renvoie uniquement le premier trouvé correspondant au selecteur css
//querySelector et querySelectorAll sont très peu performants et doivents donc être utilisés le moins possible
//s'ils doivent absolument être utilisé, ça devra être de façon sporadique , jamais dans une boucle, pour éviter des problème de performances.

//il existe getElementById qui renvoie un élement affublé d'un certain id
//ou alors on peut utiliser getElementByClassName et récupérer le premier de la collection pour répéter bien souvent le comportement de querySelector
//ici on récupere l'élement d'id li2
const image1 = document.getElementById("image1");
// pour lire un attribut d'un éllément on peut utliser getAttribute
console.log(image1.getAttribute("src")); //renvoie la src de l'image
//ou si cet attribut est natif à l'élément , on peut faire 
console.log(image1.src); //renvoie également la src de l'image

//pour modifier un attribut ou définir un nouvel attribut on peut utiliser setAttribute
image1.setAttribute('src', 'https://via.placeholder.com/150/FF0000')
//ici on a modifié l'attribut src de notre image, changeant ainsi l'image affichée
//pour  changer le style d'un élément , on peut utiliser sa propriété styke
li2.style.color = "red";
li2.style.backgroundColor = "blue"; // change la couleur de fond

//on peut également manipuler les classes d'un élément 
li2.classList.add('big'); //ajoute la class big à l'élément
//pour retiere une classe on utilise classList.remove(nomDeLaClasse)
//pour vérifier qu'une classe existe on peut utiliser classlist.
//contains(nomDeLaClasse) qui renverra true si lelement possède bien la classe demandée

//creer des éléments du DOM
//pour pouvoir créer dynamiquement des éléments et les insérer dans notre DOM, on peut utiliser document.createElement('nomDeLElement)
const nouveauLien = document.createElement('a');
nouveauLien.setAttribute('href', 'https://www.google.fr');
nouveauLien.textContent = 'Google'; //change le contenu textuel de la balise

//appendChild permet de prodiguer un nouvel Element enfant a n'importe quel element du DOM capable d'en recueillir un 
li2.appendChild(nouveauLien); //ici on insère notre lien dans notre element <li>

//pour retirer un element on peut utiliser element.remove()
//pour retirer nouveauLien il faudrait faire nouveauLien.remove()