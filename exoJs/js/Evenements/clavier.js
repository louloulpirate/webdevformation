//on attend le chargemment du DOM
document.addEventListener('DOMContentLoaded', () => {
    const usernameInput = document.getElementById('usernameInput');
    const passwordInput = document.getElementById('passwordInput');
    const submitButton = document.getElementById('submit');
    const textToModify = document.getElementById('textToModify');

    submitButton.addEventListener("click", (event) => {
        console.log(usernameInput.nodeValue, passwordInput.value)
    })

    //les evenements keydown et keyup permettent de notifier de l'appui sur une touche par l'utilisateur, et donc de régir à cet appui
    //keydown est lancé avant que la touche sois prise en compte,
    //keyup apres que la touche ait été prise en compte

    usernameInput.addEventListener("keyup", (event) => {
        textToModify.textContent = event.target.value ;
    })

    passwordInput.addEventListener("keyup", (event) => {
        const value = event.target.value;
        if (value.length < 8){
            event.target.style.borderColor = "red";
        }
        if (value.lenght >= 8 && value.lenght < 12) {
            event.target.style.borderColor = "rorange";
        } else
        if (value.length >= 12){
            event.target.style.borderColor = "green";
        }
    })
})