// Les évenements du DOM sont des notifications du navigateur lorsque quelque chose d'utile intervient lors de la navigation 
//Chaque evenement est representer par un objet de type Event contenant des propriété permettant d'obtenir des informations sur l"évenement lui même (origine, propriétés, etc...)
//Un évenement peut etre un clic, un scroll, un chargement de contenu particulier, etc.. et permet ainsi de gérer l'interaction entre l'utilisateur et le contenu , et vice versa

//par exemple, pour détecter le chargement complet du DOM, on peut utiliser un évenement DOMContentLoaded qui notifie lorsque le navigateur a terminé de consturire le DOM
//cet évènement sera indiqué à ce qu'on appele un gestionnaire d'évenemebts
//ce gestionnaire d'évènement aura pour rôle d'ecouter l'arrivée d'un evenment précis et d'executer un code lors de la dite arrivée

//le gestionnaire suivant executera notre code lorsque le DOL sera chargé, nous permettant ainsi de ne pas avoir à placer notre script à la fin de body
//document est l'élément que le gestionnaire dévenement va considerer comme son contexte(la cible)
//ici, en placant un eventListener sur document , on surveille toute la page pour guetter l'arriver d'un évenement
//DDOMContentLoaded est le type d'évenement sur lequel notre gestionnaire va réagir (ici le chargement du DOM)
//le second paramètre de addEventListener est une fonction qui s'executera lors de la réalisation de l'évenement guetté
//cette fonction pouvant être anonyme est appelée "callback"


document.addEventListener('DOMContentLoaded', () => {
//ici on définit le code à exécuter lorsque l'évenement 
//DOMcontentLoaded sera lancé
console.log("Après DOMContentLoaded", document.body); //body sera initialisé
})

//pour pouvoir cibler un élément particulier et lui ajouter un gestionnaire d'évenements (eventListener), on doit déja le récuperer dans le DOM
const boutonACliquer = document.getElementById('aCliquer');
let compteur = 0;
boutonACliquer.addEventListener('click', () => {
    compteur++;
    boutonACliquer.textContent = "cliquez moi !" + compteur;
    //ici on change le label de notre bouton pour y ajouter le nb de clics

})

console.log("Avant DOMContentLoaded", document.body) //body sera null