//OBJET
class Personnage {
    constructor(nom, pv, force, intelligence, esprit, agilite) {
        //ATTRIBUTS
        this.nom = nom
        this.pv = pv
        this.force = force
        this.intelligence = intelligence
        this.esprit = esprit
        this.agilite = agilite
        this.vivant = true
        this.xp = 0
    }

    //FONCTION/METHODE*
    description() {
        if (this.vivant) {
            return `Je suis un ${this.nom}, il me reste ${this.pv} points de vie. J'ai ${this.force} de Force, ${this.intelligence} d'intelligence, ${this.esprit} d'esprit et ${this.agilite} d'agilite. Mon compte de points d'expérience s'élève à ${this.xp}.`;
        } else {
            return '...';
        }
    }



    retirerPv(nombre) {
        this.pv -= nombre;
        if (this.pv < 0) {
            this.pv = 0;
        }
        this.vivant = this.pv > 0;
        this.estMort();
    }

    estMort() {
        this.vivant = this.pv > 0;
    }

    mourir() {
        if (this.pv <= 0) {
            this.mourir()
        }
    }

    mourir() {
        this.vivant = false
    }

    attaquer(cible) {
        if (this.vivant) {
            if (cible.vivant) {
                if ((this.force > this.intelligence) && (this.force > this.agilite)) {
                    console.log(`${this.nom} attaque ${cible.nom}`);
                    cible.retirerPv(this.force)
                    console.log(`${cible.nom} perd ${this.force} pv`);
                    return cible.pv
                }
                else {
                    if ((this.intelligence > this.force) && (this.intelligence > this.agilite)) {
                        console.log(`${this.nom} attaque ${cible.nom}`);
                        cible.retirerPv(this.intelligence)
                        console.log(`${cible.nom} perd ${this.intelligence} pv`);
                        return cible.pv;
                    }
                    else {
                        console.log(`${this.nom} attaque ${cible.nom}`);
                        cible.retirerPv(this.agilite)
                        console.log(`${cible.nom} perd ${this.agilite} pv`);
                        return cible.pv
                    }

                }

            }
            else {
                console.error(`l'ame de ${this.nom} est combattive mais rien ne se passe...`)
            }
        }
    }
    soigner(cible) {}
}

const guerrier = new Personnage("Guerrier", 150, 30, 5, 0, 10)
console.log(guerrier.description());
const mage = new Personnage("Mage", 100, 5, 30, 10, 10)
console.log(mage.description());
const voleur = new Personnage("Voleur", 120, 10, 5, 0, 30)
console.log(voleur.description());
const pretre = new Personnage("Prêtre", 100, 5, 30, 20, 10)
console.log(pretre.description())
const DragonNoir = new Personnage("Dragon Noir", 300, 30, 30, 30, 30)
console.log(DragonNoir.description());

guerrier.attaquer(DragonNoir);
mage.attaquer(DragonNoir);
voleur.attaquer(DragonNoir);
pretre.attaquer(DragonNoir);
console.log(DragonNoir.description());










//Animal extends Personnage{
   // Attributs:
    //    nb_dents: nombre positif
    //    nb_decibels: nombre positif
  //  Méthodes:
     //   Mordre: attaque qui dépent de nb_dents
   //     Intimider: attaque qui dépent de nb_decibels et de l'animal qui agit
}


class Animal extends Personnage {
    constructor (nom, pv, pm, force, nb_dents, nb_decibels){
        super (nom, pv, pm, force)
        this.nb_dents = nb_dents
        this.nb_decibels = nb_decibels
    }
    description(){
       if (this.pv > 0) {
           return `Je suis  ${this.nom}, j'ai ${this.pv} points de vie et ${this.nb_dents} dents bien aiguisées .\nJ'ai ${this.force} de force. Mon compte de points d'expérience s'élève à ${this.xp}.\n`;
       }
       else {
           return `${this.nom} erre dans les limbes de l'enfer...\n`;
       }
   }
   mordre(cible){
       cible.
   }
}

