//dans les languages orientés objet on peut utiliser une structure de données appelée objet
//celle ci permet l'organisation de données de façon efficace et systématique aidant ainsi à représenter l'existant dans notre code 

//si l'on désirait représenter un stylo on pourrait le faire de la façon suivante :
 
const stylo = {
    type: "bille",
    couleur: "bleu",
    marque: "bic",
    niveauEncre: 80 
}

//Pour afficher une propriété d'un objet comme le niveau d'encre de notre stylo) on utilise nomObjet.nomPropriete
console.log(stylo.niveauEncre);

//En programmation orientée objet, le véritable avantage découle de principe de classes
//Une classe est un shéma selon lequel on peut construire des objets
//On définit une classe en spécifiant class NomDeLaClasse {}

class Personnage {
    //Pour définir la façon d'instancier (construire ) notre personnage
    //on va décrire son constructeur
    //chaque paramètre de la fonction contructor pourra être utilisé
    //pour instancier notre objet
   
    constructor(nom, pv ,pm, force,){
        //dans cette fonction , on pourra modifier les propriétés de la classe, qui seront également les propriétés de l'objet instancié
        //this permet de cibler l'objet lui meme
        //this.nom signifie "le nom de l'objet instancié"
        //pour affecter les parametres du constructeur à notre objet , on doit utiliser this
        this.nom = nom;
        this.pv = pv;
        this.pm = pm;
        this.force = force;
        
        //on peut définir des paramètres par défaut pour tous les objets
        this.xp = 0;
        this.vivant = true;

    }

    //une classe peut posséder des fonctions, appelées méthodes, qui seront partagées par tous les objet qui en sont instance 
    //ces méthodes peuvent intéragir avec les propriétés de chaque objet respectivement
    description(){
        if(this.vivant){
        return `Je suis ${this.nom}, il me reste ${this.pv} points de vie, et ${this.pm} points de mana. J'ai ${this.force} de Force . Mon compte de points d'expérience s'élève à ${this.xp}.`;
        } else {
            return '...';
        }
    }
    //Attaquer un personnage permet d'infliger des dégats à ses points de vie en fonction de la force l'attaquant
    //ici, l'attaquant est l'objet lançant la méthode attaquer()
    //et le personnage ciblé par l'attaque est le paramètre cible
    attaquer(cible){
        //si l'attanquand est en vie
        if (this.vivant){
            //si la cible est en vie
            if (cible.vivant){
                //on fait le traitement habituel
        console.log(`${this.nom} attaque ${cible.nom} !`);
        cible.retirerPv(this.force);
        console.log(`${cible.nom} perd ${this.force} pv`);
        return cible.pv()
    } else {
        //sinon on s'épargne le retrait de pv
        console.error(`l'âme de ${this.nom} est combattive,mais rien ne se passe...`)
    } 
    }
}
    //cette méthode rétire des pv au personnage
    //et ajuste le compte si jamais celui ci passe sous le minimum 
    retirerPv(nombre){
        this.pv -= nombre; //this.pv = this.pv - nombre
        if (this.Pv < 0){
            this.pv = 0;
        }
        this.vivant = this.pv > 0
        this.estMort();
    }
    estMort(){
        this.vivant = this.pv > 0
    }
    mourir(){
        if (this.pv <= 0){
            this.mourir()
        }
    }
        //passe le statue de vivant à mort
        mourir(){
            this.vivant = false 
        }
       
            
        }
    

//pour instancier un objet à partir d'une classe
//on range dans une variable une instance de cette classe en appellant son constructeur
//pour appeler le constructeur on utilise new NomDeClasse (params)


//un objet peut etre une constante et avoir des propriétés variables
const personnage1 = new Personnage("Jones", 100, 50, 20, 5);
console.log(personnage1.description());
const personnage2 = new Personnage("Jane", 100, 80, 5, 20);
console.log(personnage2.description());

//on se crée un ennemi
const mechant = new Personnage("Georges Pompidou", 200, 0, 30, 30);
//on affiche sa description
console.log(mechant.description())
//et on l'attaque
personnage1.attaquer(mechant);
//on affiche le compte pv diminué du méchant
console.log(mechant.pv);

//HERITAGE
// le principe d'héritage en programmation orientée objet est ke faut 
// de créer une classe (enfant) dérivée d'une autre classe (mère)
// la classe enfant hérite ainsi des propriétés et méthodes de la classe mère
//pour utiliser l'héritage , on utilise le mot clé extends
class Mage extends Personnage {
    // le constructeur du mage s'écrit comme n'importe quel constructor
    // à la différence qu'il faut appeler le constructeur de la classe mère à l'aide du mot clé super
    //Ici par exemple, on définit que tous les Mages seraient des personnage avec une force de 5
    constructor(nom, pv, pm, intelligence){
        super(nom, pv, pm, 5);
        //l'intelligence quant à ell est une propriété de Mage, et non de Personnage
        this.intelligence = intelligence;
    }
//On hérite également des méthodes de la classe mère
//mais on peut y ajouter des modifications, ou les réecrire entierement, en utilisant le meme nom de méthode
    description(){
        return super.description() + `Je suis égalment très intelligent , j'ai au moins ${this.intelligence} d'intelligence!`;
    }

    //comme notre Personnage n'a pas de méthode assez générique pour infliger des dégats quelque soit leur origine, on se retrouve obligés à réécrire une bonne partie de la logique
    //si on voulait utiliser du code de notre classe mère , il aurait peut etre fallu le préparer avec une méthode permettant de préciser la source des dégats, la caractéristique utilisé..etc
    //ce qui nous aurait permis de construire lancerSort et attaquqer sur la meme base de code
    
    lancerSort(nomDuSort, cible){
        //si l'attanquand est en vie
        if (this.vivant){
            if (this.pm >= 10){
                this.pm -= 10;
            }
            //si la cible est en vie
            if (cible.vivant){
                //on fait le traitement habituel
        console.log(`${this.nom} attaque ${cible.nom} !`);
        cible.retirerPv(this.intelligence);
        console.log(`${cible.nom} perd ${this.intelligence} pv`);
        return cible.pv
    }
     else {
        console.log(`${this.nom} lance ${nomDuSort} sur le bout de viande inanimé qu'est devenu${cible.nom}! Quel gachis!`)
    }
}
        else {
        //sinon on s'épargne le retrait de pv
        console.error(`l'âme de ${this.nom} est combattive,mais rien ne se passe...`)
    } 
    }
}

const mage1 = new Mage("Gérard Majax", 80, 150, 30);
console.log(mage1.description());
mage1.lancerSort('Boule de feu', mechant);